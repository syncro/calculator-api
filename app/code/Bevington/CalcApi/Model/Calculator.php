<?php
namespace Bevington\CalcApi\Model;

use Bevington\CalcApi\Api\CalcInterface;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
 
class Calculator implements CalcInterface
{
    private $logger;

    /**
     * @param Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate($left, $right, $operator, $scale = 2)
    {
        try {
            switch (strtolower($operator)) {
                case 'add':
                    $sum = round($left + $right, $scale);
                    break;

                case 'subtract':
                    $sum = round($left - $right, $scale);
                    break;

                case 'multiply':
                    $sum = round($left * $right, $scale);
                    break;

                case 'divide':
                    $sum = round($left / $right, $scale);
                    break;

                case 'power':
                    $sum = round($left ** $right, $scale);
                    break;

                default:
                    $msg = "Please only use [add,subtract,multiply,divide,power] operators.";
                    throw new LocalizedException(__($msg));
            }

            $response = [
                'status' => 'OK',
                'result' => $sum
            ];
        } catch (LocalizedException $e) {
            $response = [
                'status' => 'FAIL',
                'result' => $e->getMessage()
            ];
        }

        try {
            return \Zend_Json::encode($response);
        } catch (LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            return;
        }
    }
}
