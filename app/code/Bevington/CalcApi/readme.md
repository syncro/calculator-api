# Bevington - CalcApi
Hi and thank you for choosing the Bevington Calc API. This is a very simple calculator extension to demonstrate the production of a Magento 2 API endpoint.

The extension has been tested on Magento 2.2 Open Source Edition.

The server spec :

* 4CPU Core 4GB RAM VPS
* Ubuntu 16.04.2 LTS
* PHP 7.0.22
* MySQL 5.7.20
* Nginx 1.10.3

The extension can be obtained via cloning or direct download from the Bit Bucket repository located here: 
`https://bitbucket.org/syncro/calculator-api`

NOTE : The next section assumes you have the testing suite correctly configured and developer mode is enabled.

Basic unit tests have been written and placed in the `<magento root>/dev/tests/api-fuctional/testsuite` directory. To run them navigate to the testsuite directory as described above and execute the command `php ../../../vendor/phpunit/phpunit/phpunit`

Alternativley you can run tests via Post Master which is a Google chrome which is great for testing REST API's.

## Request Example
* Method : POST
* Body :
`{
    "left": 1.5,
    "right": 2.5,
    "operator": "add",
    "scale": 4
}`

## Available Operators
* add (left + right)
* subtract (left - right)
* multiply (left * right)
* divide (left / right)
* power (left to the power of right)

## Success Response Example
`{
    "status": "OK",
    "result": 4
}`

## Failure Response Example
`{
    "status": "FAIL",
    "result": "message indicating issue"
}`



