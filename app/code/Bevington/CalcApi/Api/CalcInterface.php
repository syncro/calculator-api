<?php
namespace Bevington\CalcApi\Api;
 
interface CalcInterface
{
    /**
     * Returns the json encoded object with the calculation output.
     *
     * @api
     * @author David Bevington (david.bevington@syncro-it.co.uk)
     * @param  float $left
     * @param  float $right
     * @param  string $operator
     * @param  int $scale
     * @return string
     * @throws \Magento\Framework\Exception\StateException If there is a problem with the input
     */
    public function calculate($left, $right, $operator, $scale = 2);
}
